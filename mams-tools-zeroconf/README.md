# Zeroconf

Zeroconf is a simple Java implementation of Multicast DNS Service Discovery, _aka_ the service discovery bit of Zeroconf.

This is a fork of [https://github.com/faceless2/zeroconf](https://github.com/faceless2/zeroconf) that basically re-organises the library as a Maven Artifact rather than using Ant.

