# mams-tools-hateoas

Java API to support HATEOAS applications. Implements a set of classes that can be used to create Hypermedia Resources that can be transformed into [HAL](https://datatracker.ietf.org/doc/html/draft-kelly-json-hal-08) or [JSON-LD](https://www.w3.org/TR/json-ld11/).

For JSON-LD, the [Hydra](https://www.hydra-cg.com/spec/latest/core/) ontology is used to model collections.


## Getting started

The Library is built on the Jackson XML Library.  To create a Hypermedia Resource, simply extend the `mams.hypermedia.HypermediaObject` class. To get a hypermedia representation, use the `toJsonNode(...)` method.  The parameter should be a String representing the desired media type (`application/ld+json` or `application/hal+json`).

# Example: Hello World

Create a message representation that is instantiated to return the String "hello world".

## Create a hypermedia object class

```
public class Message extends HypermediaObject {
    public String id;
    public String message;

    @Override
    public String id() {
        return id; 
    }
}
```

## Instantiate the class

```
Message message = new Message();

message.id = "hello";
message.message = "Hello world";
message.setLink(HALConstants.SELF, Link.to("http://localhost/hello"));
```

## Output


The expected output (JSON-LD):

```
System.out.println(message.toJsonNode("application/ld+json"));

{
    "id": "hello",
    "message": "Hello world",
    "@id": "http://localhost/hello"
}
```

The expected output (HAL)

```
System.out.println(message.toJsonNode("application/hal+json"));


{
    "id": "hello",
    "message": "Hello world",
    "_links": {
        "self": { "href":"http://localhost/hello" }
    }
}
```
