import org.junit.Test;

import mams.hypermedia.HALConstants;
import mams.hypermedia.HypermediaCollection;
import mams.hypermedia.HypermediaObject;
import mams.hypermedia.HypermediaObject.Context;
import mams.hypermedia.HypermediaObject.Type;
import mams.hypermedia.JsonLdConstants;
import mams.hypermedia.Link;
import mams.hypermedia.transformer.HALTransformer;
import mams.hypermedia.transformer.HypermediaTransformer;
import mams.hypermedia.transformer.JsonLdTransformer;

public class BasicTest {
    @Context("http://schema.org/")
    @Type("Person")
    public static class Message extends HypermediaObject {
        public String name;

        @Override
        public String id() {
            return name;
        }
    }

    {
        HypermediaTransformer.registerTransformer(new HALTransformer());
        HypermediaTransformer.registerTransformer(new JsonLdTransformer());
    }

    @Test
    public void messageJsonLdTest() {
        HypermediaTransformer transformer = HypermediaTransformer.getTransformer(JsonLdConstants.CONTENT_TYPE);
        System.out.println(transformer.transform(exampleMessage()));
    }

    @Test
    public void messageHALTest() {
        HypermediaTransformer transformer = HypermediaTransformer.getTransformer(HALConstants.CONTENT_TYPE);
        System.out.println(transformer.transform(exampleMessage()));
    }

    private HypermediaCollection<Message> exampleCollection() {
        HypermediaCollection<Message> collection = new HypermediaCollection<>("messages");
        collection.add(exampleMessage());
        return collection;
    }

    private Message exampleMessage() {
        Message message = new Message();

        message.name = "Hello world";
        message.setLink(HALConstants.SELF, Link.to("http://localhost/hello"));
        return message;
    }

    @Test
    public void collectionJsonLdTest() {
        HypermediaTransformer transformer = HypermediaTransformer.getTransformer(JsonLdConstants.CONTENT_TYPE);
        System.out.println(transformer.transform(exampleCollection()));
    }

    @Test
    public void collectionHALTest() {
        HypermediaTransformer transformer = HypermediaTransformer.getTransformer(HALConstants.CONTENT_TYPE);
        System.out.println(transformer.transform(exampleCollection()));
    }
}
