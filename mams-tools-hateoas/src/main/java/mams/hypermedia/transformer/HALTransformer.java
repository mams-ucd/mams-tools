package mams.hypermedia.transformer;

import java.lang.reflect.Field;
import java.util.Map.Entry;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import mams.hypermedia.HALConstants;
import mams.hypermedia.HypermediaCollection;
import mams.hypermedia.HypermediaObject;
import mams.hypermedia.Link;

public class HALTransformer extends HypermediaTransformer {
    static {
        HypermediaTransformer.registerTransformer(new HALTransformer());
        System.out.println("Added HAL Transformer");
    }

    public String type() {
        return HALConstants.CONTENT_TYPE;
    }

    @Override
    public <T extends HypermediaObject> JsonNode transform(T object) {
        ObjectNode node = mapper.createObjectNode();
        for (Field field : object.getClass().getFields()) {
            try {
                if (HypermediaObject.class.isAssignableFrom(field.getType())) {
                    node.set(field.getName(), transform((HypermediaObject) field.get(object)));
                } else {
                    node.set(field.getName(), mapper.valueToTree(field.get(object)));
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        ObjectNode _links = mapper.createObjectNode();
        for (Entry<String, Link> entry : object.linkEntries()) {
            ObjectNode self = mapper.createObjectNode();
            self.put(HALConstants.HREF, entry.getValue().href);
            _links.set(entry.getKey(), self);

        }
        node.set(HALConstants.LINKS, _links);

        if (HypermediaCollection.class.isAssignableFrom(object.getClass())) {
            ArrayNode array = mapper.createArrayNode();
            for (HypermediaObject element : ((HypermediaCollection<?>) object).elements()) {
                array.add(transform(element));
            }
    
            ObjectNode list = mapper.createObjectNode();
            list.set(object.id(), array);
            node.set(HALConstants.EMBEDDED, list);
        }
        return node;
    }
}
