package mams.hypermedia.transformer;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import mams.hypermedia.HypermediaObject;

public abstract class HypermediaTransformer {
    protected static ObjectMapper mapper = new ObjectMapper();
    private static Map<String, HypermediaTransformer> transformers = new HashMap<>();
    private static String defaultType;

    public static void registerTransformer(HypermediaTransformer transformer) {
        if (defaultType == null) defaultType = transformer.type();
        transformers.put(transformer.type(), transformer);
    }

    public static HypermediaTransformer getTransformer(String type) {
        return transformers.get(type);
    }

    public abstract String type();
    public abstract <T extends HypermediaObject> JsonNode transform(T object);
}
