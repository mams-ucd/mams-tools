package mams.hypermedia.transformer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Map.Entry;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import mams.hypermedia.HALConstants;
import mams.hypermedia.HydraConstants;
import mams.hypermedia.HypermediaCollection;
import mams.hypermedia.HypermediaObject;
import mams.hypermedia.HypermediaObject.Context;
import mams.hypermedia.HypermediaObject.Type;
import mams.hypermedia.JsonLdConstants;
import mams.hypermedia.Link;

public class JsonLdTransformer extends HypermediaTransformer {
    static {
        HypermediaTransformer.registerTransformer(new JsonLdTransformer());
        System.out.println("Added JsonLd Transformer");
    }

    public String type() {
        return JsonLdConstants.CONTENT_TYPE;
    }

    @Override
    public <T extends HypermediaObject> JsonNode transform(T object) {
        ObjectNode node = mapper.createObjectNode();
        for (Field field :  object.getClass().getFields()) {
            try {
                if (HypermediaObject.class.isAssignableFrom(field.getType())) {
                    node.set(field.getName(), transform((HypermediaObject) field.get(object)));
                } else {
                    node.set(field.getName(), mapper.valueToTree(field.get(object)));
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        
        for (Annotation annotation : object.getClass().getAnnotations()) {
            if (annotation.annotationType().equals(Context.class)) {
                Context context = (Context) annotation;
                node.put(JsonLdConstants.CONTEXT, context.value());
            } else if (annotation.annotationType().equals(Type.class)) {
                Type type = (Type) annotation;
                node.put(JsonLdConstants.TYPE, type.value());
            }

        }
        for (Entry<String, Link> entry : object.linkEntries()) {
            if (entry.getKey().equals(HALConstants.SELF)) {
                node.put(JsonLdConstants.ID, entry.getValue().href);
            } else {
                node.put(entry.getKey(), entry.getValue().href);
            }
        }

        if (HypermediaCollection.class.isAssignableFrom(object.getClass())) {
            ArrayNode array = mapper.createArrayNode();
            for (HypermediaObject element : ((HypermediaCollection<?>) object).elements()) {
                array.add(transform(element));
            }
            
            node.put(JsonLdConstants.CONTEXT, HydraConstants.CONTEXT);
            node.put(JsonLdConstants.TYPE, HydraConstants.COLLECTION);
            node.put(HydraConstants.TOTAL_ITEMS, array.size());
            node.set(HydraConstants.MEMBER, array);
        }
        return node;
    }
    
}
