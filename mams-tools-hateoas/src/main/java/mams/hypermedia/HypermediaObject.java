package mams.hypermedia;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class HypermediaObject {
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    public @interface Context {
        String value() default "";
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    public @interface Type {
        String value() default "";
    }

    protected static ObjectMapper mapper = new ObjectMapper();
    private Map<String, Link> links = new HashMap<>();

    // public HypermediaObject(String slug) {
    //     setLink(SELF_ID, new Link(slug));
    // }
    
    public abstract String id();

    public HypermediaObject setLink(String key, Link link) {
        links.put(key, link);
        return this;
    }

    public Link link(String key) {
        return links.get(key);
    }

    public Set<Entry<String, Link>> linkEntries() {
        return links.entrySet();
    }

    public void copyLinks(HypermediaObject object) {
        for (Entry<String, Link> entry : object.links.entrySet()) {
            links.put(entry.getKey(), entry.getValue());
        }
    }
}
