package mams.hypermedia;

public class Link {
    private static String baseUrl = "http://localhost";

    public static void baseUrl(String url) {
        if (url.endsWith("/")) url = url.substring(0,url.length()-1);
        baseUrl = url;
    }

    protected Link(String href) {
        this.href = href;
    }
    public Link() {}
    
    public String href;

    public static Link absolute(String href) {
        if (!href.startsWith("/")) href = "/"+href;
        return new Link(baseUrl+href);
    }

    public static Link relative(String href) {
        if (!href.startsWith("/")) href = "/"+href;
        return new Link(href);
    }

    public static Link to(String href) {
        return new Link(href);
    }
}
