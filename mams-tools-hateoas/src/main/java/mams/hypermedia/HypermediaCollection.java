package mams.hypermedia;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class HypermediaCollection<T extends HypermediaObject> extends HypermediaObject {
    private List<T> elements = new LinkedList<T>();
    
    private String id;

    public HypermediaCollection(String id) {
        this.id = id;
        setLink(HALConstants.SELF, Link.absolute(id));
    }

    public String id() {
        return id;
    }

    public HypermediaCollection<T> add(T element) {
        elements.add(element);
        element.setLink(HALConstants.SELF, Link.absolute(id+"/"+element.id()));
        element.setLink(HALConstants.PARENT, link(HALConstants.SELF));
        return this;
    }

    public T get(String id) {
        for (T element : elements) {
            if (element.id().equals(id)) return element;
        }
        return null;
    }

    public void addAll(Collection<T> values) {
        elements.addAll(values);
    }

    public List<T> elements() {
        return elements;
    }
}
