package mams.hypermedia;

public class HALConstants {
    public static final String CONTENT_TYPE = "application/hal+json";
    public static final String LINKS = "_links";
    public static final String HREF = "href";
    public static final String EMBEDDED = "_embedded";

    public static final String SELF = "self";
    public static final String PARENT = "parent";
}
