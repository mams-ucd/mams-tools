package mams.hypermedia;

public class JsonLdConstants {
    public static final String CONTENT_TYPE = "application/ld+json";
    public static final String CONTEXT = "@context";
    public static final String ID = "@id";
    public static final String TYPE = "@type";
}
