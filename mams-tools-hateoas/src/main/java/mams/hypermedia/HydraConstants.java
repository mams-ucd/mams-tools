package mams.hypermedia;

public class HydraConstants {  
    public static final String CONTEXT = "https://www.markus-lanthaler.com/hydra/core";
    // public static final String CONTEXT = "http://www.w3.org/ns/hydra/context.jsonld";
    public static final String COLLECTION = "Collection";
    public static final String TOTAL_ITEMS = "totalItems";
    public static final String MEMBER = "member";
}
