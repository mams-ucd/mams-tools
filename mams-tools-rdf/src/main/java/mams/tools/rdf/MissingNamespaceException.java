package mams.tools.rdf;

public class MissingNamespaceException extends RuntimeException {
    public MissingNamespaceException(String msg) {
        super(msg);
    }

    public MissingNamespaceException(String msg, Throwable th) {
        super(msg, th);
    }

}
