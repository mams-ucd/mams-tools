package mams.tools.rdf;
import static org.eclipse.rdf4j.model.util.Values.iri;
import static org.eclipse.rdf4j.model.util.Values.literal;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.apache.commons.io.output.WriterOutputStream;
import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.RDFCollections;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;

import mams.tools.rdf.annotations.RdfNamespace;
import mams.tools.rdf.annotations.RdfNamespaces;
import mams.tools.rdf.annotations.RdfObject;
import mams.tools.rdf.annotations.RdfPredicate;
import mams.tools.rdf.annotations.RdfType;

public class Transformer {
    private static Map<Class<?>, Function<Literal, Object>> converters = new HashMap<>();

    static {
        converters.put(String.class, literal -> { return literal.stringValue(); });
        converters.put(Integer.class, literal -> { return literal.integerValue(); });
    }

    public Model readRDF(RDFFormat format, String content, String iri) throws RDFParseException, UnsupportedRDFormatException, IOException {
        return readRDF(format, new ByteArrayInputStream(content.getBytes()), iri);
    }

    public Model readRDF(RDFFormat format, InputStream inputStream, String iri) throws RDFParseException, UnsupportedRDFormatException, IOException {
        return Rio.parse(inputStream, iri, RDFFormat.TURTLE);
    }

    public String writeRDF(RDFFormat format, Model model){
        StringWriter writer = new StringWriter();
        try {
            WriterOutputStream out = new WriterOutputStream(writer, Charset.defaultCharset());
            RDFWriter rdfWriter = Rio.createWriter(format, out);
            rdfWriter.startRDF();
            for (Namespace namespace : model.getNamespaces()) {
                rdfWriter.handleNamespace(namespace.getPrefix(), namespace.getName());
            }
            for (Statement st : model) {
                rdfWriter.handleStatement(st);
            }
            rdfWriter.endRDF();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return writer.toString();
    }

    public <T> T transform(Model model, Class<T> cls) {
        // Comment out code below to view the model
        // System.out.println("\n\nStatements:");
        // for (Statement statement : model.getStatements(null, null, null)) {
        //     System.out.println(statement.toString());
        // }

        T object = null;
        try {
            object = cls.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        Map<String, String> namespaces = new HashMap<String, String>();
        /* Set the TYPE and PREFIX for the model using the annotations*/

        for (Annotation annotation : cls.getAnnotations()) {
            if (RdfNamespaces.class.isInstance(annotation)) {
                RdfNamespaces prefixes = (RdfNamespaces) annotation;
                for (RdfNamespace prefix : prefixes.value()) {
                    namespaces.put(prefix.label(), prefix.vocab());
                }
            } else if (RdfType.class.isInstance(annotation)) {
                RdfType type = (RdfType) annotation;
                String[] bits = type.value().split(":");
                String qname = namespaces.get(bits[0]);
                IRI typeIRI = qname == null ? iri(type.value()):iri(qname, bits[1]);
                if (!model.contains(null, RDF.TYPE, typeIRI)) {
                    throw new MissingNamespaceException("Model does not contain type: " + typeIRI);
                }
            }
        }

        // Check for namespaces... (commented out because it does not work on submodels)
        // for (Entry<String,String> entry : namespaces.entrySet()) {
        //     Optional<Namespace> namespace = model.getNamespace(entry.getKey());
        //     if (!namespace.isPresent()) {
        //         throw new MissingNamespaceException("Model does not contain namespace: " + entry.getKey());
        //     }
        // }

        // Now extract values from triples
        Field[] classMemberFields = object.getClass().getDeclaredFields();
        for(Field field : classMemberFields){
            field.setAccessible(true);
            try {
                Annotation annotation = field.getAnnotation(RdfPredicate.class);
                RdfPredicate predicate = (RdfPredicate) annotation;
                String[] bits = predicate.value().split(":");
                String qname = namespaces.get(bits[0]);
                IRI pred = qname == null ? iri(predicate.value()):iri(qname, bits[1]);

                for (Resource resource : model.filter(null, pred, null).subjects()) {
                    if (field.getType().equals(IRI.class)) {
                        field.set(object, Models.getPropertyIRI(model, resource, pred).get()); 
                    } else if (field.getAnnotation(RdfObject.class) != null) {
                        Value value = Models.getProperty(model, resource, pred).get();
                        if (!value.isBNode()) {
                            throw new MissingNamespaceException("Expected BNode for: " + field.getName() + " but got: " + value);
                        }
                        Object obj = transform(model.filter(((BNode) value), null, null), field.getType());
                        field.set(object, obj);
                    } else {
                        try {
                            Literal literal = Models.getPropertyLiteral(model, resource, pred).get();
                            if (converters.containsKey(field.getType())) {
                                field.set(object, converters.get(field.getType()).apply(literal));
                            } else {
                                throw new UnsupportedOperationException("No converter for: " + field.getType());
                            }
                        } catch (IllegalArgumentException e) {
                            throw new MissingNamespaceException("Could not map: " + Models.getPropertyLiteral(model, resource, pred).get()
                                                + " to: " + field.getName() + " with type: " + field.getType(), e);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        return object;
    }

    public Model transform(IRI iri, Object object){
        ModelBuilder builder = new ModelBuilder();
        transform(builder, iri, object);

        return builder.build();
    }

    private void transform(ModelBuilder builder, Resource subject, Object object) {
        builder.subject(subject);
        Map<String, String> namespaces = new HashMap<String, String>();
        /* Set the TYPE and PREFIX for the model using the annotations*/

        for (Annotation annotation : object.getClass().getAnnotations()) {
            if (RdfNamespaces.class.isInstance(annotation)) {
                RdfNamespaces prefixes = (RdfNamespaces) annotation;
                for (RdfNamespace prefix : prefixes.value()) {
                    builder.setNamespace(prefix.label(), prefix.vocab());
                    namespaces.put(prefix.label(), prefix.vocab());
                }
            } else if (RdfType.class.isInstance(annotation)) {
                RdfType type = (RdfType) annotation;
                String[] bits = type.value().split(":");
                String qname = namespaces.get(bits[0]);
                IRI obj = qname == null ? iri(type.value()):iri(qname, bits[1]);
                builder.add(RDF.TYPE, obj);
            }
        }
    
        /* Set the variable values in the model using reflection to call the Getter - could be 
        an issue when it returns an object */
        Field[] classMemberFields = object.getClass().getDeclaredFields();
        for(Field field : classMemberFields){
            field.setAccessible(true);
            Annotation annotation = field.getAnnotation(RdfPredicate.class);
            if (annotation != null) {
                try {
                    RdfPredicate predicate = (RdfPredicate) annotation;

                    Object obj = field.get(object);
                    if (obj instanceof IRI) {
                        builder.add(predicate.value(), obj);
                    } else if (obj instanceof Collection) {
                        if (field.getAnnotation(RdfObject.class) == null) {
                            // Use the built in functionaltiy
                            List<Value> values = new ArrayList<>();
                            Resource head = Values.bnode();
                            ((Collection<?>) obj).forEach(o-> {
                                if (o instanceof IRI) {
                                    values.add((IRI) o);
                                } else {
                                    values.add(literal(o));
                                }
                            });
                            RDFCollections.asRDF(values, head, new LinkedHashModel()).forEach(
                                stmt -> {
                                    builder.add(stmt.getSubject(), stmt.getPredicate(), stmt.getObject());
                                }
                            );
                            builder.subject(subject);
                            builder.add(predicate.value(), head);
                        } else {
                            Resource head = Values.bnode();
                            builder.add(predicate.value(), head);
                            builder.subject(head);
                            builder.add(RDF.TYPE, RDF.LIST);
    
                            Resource current = head;
                            int i = 0;
                            for (Object o : ((Collection<?>) obj)) {
                                Resource body = Values.bnode();
                                builder.add(RDF.FIRST, body);
    
                                // Create and add the body
                                ModelBuilder listBuilder = new ModelBuilder();
                                transform(builder, body, o);
                                builder.subject(body);
                                listBuilder
                                    .build()
                                    .forEach(stmt -> {
                                        builder.add(stmt.getPredicate(), stmt.getObject());
                                    });
    
                                builder.subject(current);
                                if (++i == ((Collection<?>) obj).size()) {
                                    builder.add(RDF.REST, RDF.NIL);
                                } else {
                                    current = Values.bnode();
                                    builder.add(RDF.REST, current);
                                    builder.subject(current);
                                }
    
                            }
                        }
                    } else if (field.getAnnotation(RdfObject.class) != null) {
                        BNode address = Values.bnode();
                        builder.add(predicate.value(), address);
                        transform(builder, address, obj);
                        builder.subject(subject);
                    } else {
                        builder.add(predicate.value(), literal(obj));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}