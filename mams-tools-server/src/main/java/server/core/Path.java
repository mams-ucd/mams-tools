package server.core;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;

public class Path {
    private Map<HttpMethod, Consumer<HttpRequestState>> handlers = new HashMap<>();
    private String[] path;

    public Path get(Consumer<HttpRequestState> consumer) { return set(HttpMethod.GET, consumer); }
    public Path post(Consumer<HttpRequestState> consumer) { return set(HttpMethod.POST, consumer); }
    public Path put(Consumer<HttpRequestState> consumer) { return set(HttpMethod.PUT, consumer); }
    public Path patch(Consumer<HttpRequestState> consumer) { return set(HttpMethod.PATCH, consumer); }
    public Path delete(Consumer<HttpRequestState> consumer) { return set(HttpMethod.DELETE, consumer); }
    public Path head(Consumer<HttpRequestState> consumer) { return set(HttpMethod.HEAD, consumer); }

    public Path set(HttpMethod method, Consumer<HttpRequestState> consumer) {
        handlers.put(method, consumer);
        return this;
    }

    {
        set(HttpMethod.OPTIONS, state -> {
            StringBuilder builder = new StringBuilder();
            boolean first = true;
            for (HttpMethod method : handlers.keySet()) {
                if (first) first=false; else builder.append(",");
                builder.append(method.asciiName());
            }

            ResponseEntity entity = new ResponseEntity(HttpResponseStatus.NO_CONTENT).header("Allow", builder.toString());
            WebServer.writeResponse(state, entity);
        });
    }

    public Path(String path) {
        if (path.startsWith("/")) path = path.substring(1);
        if (path.endsWith("/")) path = path.substring(0, path.length()-1);

        this.path = path.split("/");
    }

    public PathBinding bind(RequestPath requestPath) {
        Map<String, String> bindings = new HashMap<>();

        String[] fullPath = requestPath.fullPath();
        if (path.length != fullPath.length)
            return PathBinding.FAILED;

        for (int i=0; i<fullPath.length; i++) {
            if (path[i].startsWith("{")) {
                bindings.put(path[i].substring(1, path[i].length()-1), fullPath[i]);
            } else if (!path[i].equals(fullPath[i])) {
                return PathBinding.FAILED;
            }
        }
        
        return PathBinding.create(bindings);
    }

    public void handle(ChannelHandlerContext ctx, FullHttpRequest request, PathBinding bindings) {
        Consumer<HttpRequestState> consumer = this.handlers.get(request.method());
        if (consumer == null) {
            WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.METHOD_NOT_ALLOWED);
        } else {
            String contentType = Utils.contentNegotiation(request);
            System.out.println("Content-Type: " + contentType);
            if (contentType == null) {
                WebServer.writeErrorResponse(ctx, request, HttpResponseStatus.UNSUPPORTED_MEDIA_TYPE);
            } else {
                consumer.accept(new HttpRequestState(ctx, request, bindings, contentType));
            }
        }
    }

    public static Path create(String path) {
        Path newPath = new Path(path);
        WebServer.getInstance().addPath(newPath);
        return newPath;
    }

    public static Path create(WebServer webServer, String path) {
        Path newPath = new Path(path);
        webServer.addPath(newPath);
        return newPath;
    }

    public String toString() {
        return Arrays.asList(path).toString();
    }
}
