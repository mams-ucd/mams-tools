package server.core;

import java.util.Map;

public class PathBinding {
    public static final PathBinding FAILED = new PathBinding();

    public static PathBinding create(Map<String, String> bindings) {
        return new PathBinding(bindings);
    }

    private Map<String, String> bindings;  

    protected PathBinding(Map<String, String> bindings) {
        this.bindings = bindings;
    }

    protected PathBinding() {}

    public boolean isMatch() {
        return bindings != null;
    }

    public String get(String key) {
        return bindings.get(key);
    }
}
