package server.core;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.FullHttpRequest;

public class Utils {
    public static String getBody(FullHttpRequest request) throws IOException {
        ByteBuf buf = request.content();
        byte[] bytes = new byte[buf.readableBytes()];
        int readerIndex = buf.readerIndex();
        buf.getBytes(readerIndex, bytes);
        return new String(bytes, "UTF-8");
    }

    public static ByteBuf toByteBuf(String content) {
        byte[] bytes = content.getBytes(StandardCharsets.UTF_8);
		return Unpooled.wrappedBuffer(bytes);
    }

    public static final String DEFAULT_CONTENT_TYPE = "application/json";
    public static String defaultContentType = DEFAULT_CONTENT_TYPE;
    public static Set<String> contentTypes = new HashSet<>(Arrays.asList(DEFAULT_CONTENT_TYPE));
    
    public static String contentNegotiation(FullHttpRequest request) {
        System.out.println("Accept: " + request.headers().get("Accept"));
        String[] accepts = request.headers().get("Accept").split(",");
        for (String accept : accepts) {
            String[] bits = accept.split(";");
            if (contentTypes.contains(bits[0].trim())) return accept;
            if (bits[0].trim().equals("*/*")) return defaultContentType;
        }
        return null;
    }
}
