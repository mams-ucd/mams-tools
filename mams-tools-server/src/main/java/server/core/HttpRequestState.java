package server.core;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

public class HttpRequestState {
    public ChannelHandlerContext ctx;
    public FullHttpRequest request;
    public PathBinding binding;
    public String contentType;

    public HttpRequestState(ChannelHandlerContext ctx, FullHttpRequest request, PathBinding binding, String contentType) {
        this.ctx = ctx;
        this.request = request;
        this.binding = binding;
        this.contentType = contentType;
    }
}