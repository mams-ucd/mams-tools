package server.core;

import java.util.List;
import java.util.LinkedList;

public class RequestPath {
    private List<String> path;
    
    public RequestPath(String uri) {
        path = new LinkedList<String>();
        for (String segment: uri.split("/")) {
            if (segment.length() > 0) path.add(segment);
        }
    }
    
    public boolean pathEnd() {
        return path.size() == 1;
    }
    
    public String prefix() {
        return path.remove(0);
    }

    public String[] fullPath() {
        return path.toArray(new String[path.size()]);
    }
    
    public int length() {
        return path.size();
    }
    
    public String toString() {
        return path.toString();
    }
}